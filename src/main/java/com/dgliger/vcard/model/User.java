package com.dgliger.vcard.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.URL;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
@Entity
@Table(name = "user", indexes = @Index(name = "uniqueIndex", columnList = "employeeId,mailId", unique = true))
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String employeeId;
    private String name;
    private String designation;
    @Size(min=0,max=10)
    private String mobNumber;
    @Email
    private String mailId;
    @Size(min=0,max=10)
    private String whatsappNumber;
    private String location;
   // @URL
    private String website;
}
