package com.dgliger.vcard.model.response;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class UserResponse {


    private Long id;
    private String employeeId;
    private String name;
    private String designation;
    private String mobNumber;
    private String mailId;
    private String whatsappNumber;
    private String location;
    private String website;
}
