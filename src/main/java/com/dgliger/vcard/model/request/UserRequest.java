package com.dgliger.vcard.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserRequest {

    private String employeeId;
    private String name;
    private String designation;
    private String mobNumber;
    private String mailId;
    private String whatsappNumber;
    private String location;
    private String website;
}
