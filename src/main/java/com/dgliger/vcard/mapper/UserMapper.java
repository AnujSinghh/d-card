package com.dgliger.vcard.mapper;


import com.dgliger.vcard.model.User;
import com.dgliger.vcard.model.request.UserRequest;
import com.dgliger.vcard.model.response.UserResponse;
import lombok.experimental.UtilityClass;

@UtilityClass
public class UserMapper {

    public UserResponse mapUserToUserResponse(User user){
        return UserResponse.builder()
                .id(user.getId())
                .employeeId(user.getEmployeeId())
                .name(user.getName())
                .designation(user.getDesignation())
                .mobNumber(user.getMobNumber())
                .mailId(user.getMailId())
                .whatsappNumber(user.getWhatsappNumber())
                .location(user.getLocation())
                .website(user.getWebsite())
                .build();
    }


    public User mapUserRequestToUser(UserRequest userRequest){

        return User.builder()
                .employeeId(userRequest.getEmployeeId())
                .name(userRequest.getName())
                .designation(userRequest.getDesignation())
                .mobNumber(userRequest.getMobNumber())
                .mailId(userRequest.getMailId())
                .whatsappNumber(userRequest.getWhatsappNumber())
                .location(userRequest.getLocation())
                .website(userRequest.getWebsite())
                .build();
    }
}
