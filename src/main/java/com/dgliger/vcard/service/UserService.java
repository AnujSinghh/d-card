package com.dgliger.vcard.service;

import com.dgliger.vcard.mapper.UserMapper;
import com.dgliger.vcard.model.User;
import com.dgliger.vcard.model.request.UserRequest;
import com.dgliger.vcard.model.response.UserResponse;
import com.dgliger.vcard.repository.UserRepository;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageConfig;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@Service
public class UserService {

    private UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public UserResponse createUser(UserRequest userRequest) {
        return UserMapper.mapUserToUserResponse(userRepository.save(UserMapper.mapUserRequestToUser(userRequest)));
    }

    public UserResponse getUserCard(String employeeId) {
        return UserMapper.mapUserToUserResponse(userRepository.findByEmployeeId(employeeId));
    }

    public String generateQR(String employeeId) throws WriterException, IOException, MessagingException {
        User user = userRepository.findByEmployeeId(employeeId);
        Map hints = new HashMap();
        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
        QRCodeWriter writer = new QRCodeWriter();
        BitMatrix bitMatrix = null;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            bitMatrix = writer.encode("http://3.95.36.75:8022/api/v1/users/" + employeeId, BarcodeFormat.QR_CODE, 500, 500, hints);
            MatrixToImageConfig config = new MatrixToImageConfig(MatrixToImageConfig.BLACK, MatrixToImageConfig.WHITE);
            // Load QR image
            BufferedImage qrImage = MatrixToImageWriter.toBufferedImage(bitMatrix, config);
            // Load logo image
            File file = new File("/home/amulya/amulya/dg-business-card/QR-logo.png");
            BufferedImage logoImage = ImageIO.read(file);
            // Calculate the delta height and width between QR code and logo
            int deltaHeight = qrImage.getHeight() - logoImage.getHeight();
            int deltaWidth = qrImage.getWidth() - logoImage.getWidth();
            // Initialize combined image
            BufferedImage combined = new BufferedImage(qrImage.getHeight(), qrImage.getWidth(), BufferedImage.TYPE_INT_ARGB);
            Graphics2D g = (Graphics2D) combined.getGraphics();
            // Write QR code to new image at position 0/0
            g.drawImage(qrImage, 0, 0, null);
            g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1f));
            // Write logo into combine image at position (deltaWidth / 2) and
            // (deltaHeight / 2). Background: Left/Right and Top/Bottom must be
            // the same space for the logo to be centered
            g.drawImage(logoImage, (int) Math.round(deltaWidth / 2), (int) Math.round(deltaHeight / 2), null);
            // Write combined image as PNG to OutputStream
            ImageIO.write(combined, "png", new File("/home/amulya/amulya/dg-business-card/QR-images" + user.getName() + "-QR.jpg"));
            System.out.println("done");
        } catch (Exception e) {
            System.out.println(e);
        }
        System.out.println("preparing to send message ...");
        String message = "Hi " + user.getName() + ",\n\n Please find attached the QR code for your business-card. \n\n\n Regards,\nAnuj Singh\nM. +91-8384061273";
        String subject = "DGLiger : Business-card-QR";
        String to = user.getMailId();
        String from = "anujrajput1307@gmail.com";
        sendAttach(message, subject, to, from, user);

        return "QR code for " + user.getName() + " generated and sent to his/her mail successfully !!!";
    }

    private static void sendAttach(String message, String subject, String to, String from, User user) throws MessagingException {

        String host = "smtp.gmail.com";

        Properties properties = System.getProperties();
        System.out.println("PROPERTIES " + properties);

        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", "465");
        properties.put("mail.smtp.ssl.enable", "true");
        properties.put("mail.smtp.auth", "true");

        Session session = Session.getInstance(properties, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("anujrajput1307@gmail.com", "pefphysalxvhocdw");
            }
        });

        session.setDebug(true);

        MimeMessage m = new MimeMessage(session);

        try {
            m.setFrom(from);
            m.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            m.setSubject(subject);

//            String path = "C://Users//dglig//.vscode//Digital_V_Card//images//" + user.getName() + "-QR.jpg";
            String path = "/home/amulya/amulya/dg-business-card/QR-images" + user.getName() + "-QR.jpg";
            MimeMultipart mimeMultipart = new MimeMultipart();

            MimeBodyPart textMime = new MimeBodyPart();
            MimeBodyPart fileMime = new MimeBodyPart();

            try {
                textMime.setText(message);

                File file = new File(path);
                fileMime.attachFile(file);

                mimeMultipart.addBodyPart(textMime);
                mimeMultipart.addBodyPart(fileMime);
            } catch (Exception e) {
                e.printStackTrace();
            }

            m.setContent(mimeMultipart);

        } catch (Exception e) {
            e.printStackTrace();
        }
        Transport.send(m);
    }
}
