package com.dgliger.vcard.controllers;

import com.dgliger.vcard.model.request.UserRequest;
import com.dgliger.vcard.model.response.UserResponse;
import com.dgliger.vcard.service.UserService;
import com.google.zxing.WriterException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.mail.MessagingException;
import javax.validation.Valid;
import java.io.IOException;

@RestController
@RequestMapping("/api/v1/users")
public class UserController {

    private UserService userService;

    public UserController(UserService userService){
        this.userService=userService;
    }



    @PostMapping
    public UserResponse createUser(@Valid @RequestBody UserRequest userRequest){
       return userService.createUser(userRequest);
    }



    @GetMapping("/{employeeId}")
    public ModelAndView getUserCard(@PathVariable("employeeId") String employeeId) {
        UserResponse userResponse = userService.getUserCard(employeeId);
        ModelAndView mav = new ModelAndView("user");
        mav.addObject("name", userResponse.getName());
        mav.addObject("designation",userResponse.getDesignation());
        mav.addObject("mobNumber",userResponse.getMobNumber());
        mav.addObject("mailId",userResponse.getMailId());
        mav.addObject("whatsappNumber",userResponse.getWhatsappNumber());
        mav.addObject("location",userResponse.getLocation());
        mav.addObject("website",userResponse.getWebsite());
        mav.addObject("callUrl","tel:+91"+userResponse.getMobNumber());
        mav.addObject("mailUrl","mailto:"+userResponse.getMailId()+"?subject=DGLiger%20-%20Business%20Card&body=Hi%20"+userResponse.getName()+", %0D%0A%0D%0AGot%20your%20reference%20from%20your%20*Digital%20Business%20Card*.%20I%20want%20to%20know%20more%20about%20your%20services.");
        mav.addObject("whatsappUrl","https://api.whatsapp.com/send?phone=91"+userResponse.getWhatsappNumber()+"&text=Hi%20"+userResponse.getName()+", Got%20your%20reference%20from%20your%20*Digital%20Business%20Card*.%20I%20want%20to%20know%20more%20about%20your%20services.");
        return mav;
    }

    @GetMapping("/generate-qr/{employeeId}")
    public String generateQR(@PathVariable("employeeId") String employeeId)  throws WriterException, IOException, MessagingException {
       return userService.generateQR(employeeId);
    }


}
