package com.dgliger.vcard.repository;

import com.dgliger.vcard.model.User;
import com.dgliger.vcard.model.response.UserResponse;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User,Long> {
    User findByEmployeeId(String employeeId);
}
